/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.dao.generator;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Schema;

/**
 * Generates db classes.
 *
 * @author Doruk Kangal
 */
final class GeoReminderDaoGenerator {

    private static final int VERSION = 1;
    private static final String TARGET_PACKAGE = "com.dorukkangal.georeminder.android.green.dao";
    private static final String TARGET_PATH = "../GeoReminder/src-gen";

    /**
     * Don't init.
     */
    private GeoReminderDaoGenerator() {
    }

    public static void main(String[] args) {
        final Schema schema = new Schema(VERSION, TARGET_PACKAGE);
        new GeoReminderEntity(schema);

        try {
            new DaoGenerator().generateAll(schema, TARGET_PATH);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
