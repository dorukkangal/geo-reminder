/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.dao.generator;

import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

/**
 * @author Doruk Kangal
 */
class GeoReminderEntity {

    private static final String TABLE = "geo_reminder";

    private static final String NAME = "name";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";

    private Entity entity;

    public GeoReminderEntity(Schema schema) {

        if (entity != null && schema.getEntities().contains(entity)) {
            throw new IllegalStateException("Only one instance of this class can live in the same schema");
        }
        createEntity(schema);
    }

    /**
     * Instantiates entity and invokes property assignment.
     *
     * @param schema to which entity will be added.
     */
    private void createEntity(Schema schema) {
        entity = schema.addEntity(getClass().getSimpleName());
        addProperties(entity);
    }

    protected void addProperties(Entity entity) {
        entity.setTableName(TABLE);

        entity.addIdProperty().autoincrement();
        entity.addStringProperty(NAME).unique();
        entity.addDoubleProperty(LATITUDE);
        entity.addDoubleProperty(LONGITUDE);
    }

    public Entity getEntity() {
        return entity;
    }
}
