/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.view.list;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;

import com.dorukkangal.georeminder.android.view.base.BaseMvvmActivity;
import com.dorukkangal.georeminder.android.view.base.BaseView;
import com.dorukkangal.georeminder.android.view.base.BaseViewModel;

/**
 * @author Doruk Kangal
 */
public class GeoReminderListActivity extends BaseMvvmActivity implements BaseView {

    public static Intent newIntent(Context context) {
        return new Intent(context, GeoReminderListActivity.class);
    }

    @Override
    protected Fragment getContainedFragment() {
        return GeoReminderListFragment.newInstance();
    }

    @Override
    protected BaseViewModel createViewModel() {
        return null;
    }
}
