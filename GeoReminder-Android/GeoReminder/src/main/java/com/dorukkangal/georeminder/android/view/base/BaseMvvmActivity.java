/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.view.base;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;

import com.dorukkangal.georeminder.android.objectgraph.ComponentProvider;

/**
 * A {@link BaseActivity} that uses an {@link BaseViewModel}
 * to implement a Model-View-ViewModel architecture.
 *
 * @param <VM> indicates the viewModel responsible to manage this activity.
 * @author Doruk Kangal
 */
public abstract class BaseMvvmActivity<VM extends BaseViewModel>
        extends BaseActivity implements BaseView {

    /**
     * The view model class for this activity.
     * Will be instantiated with {@link #createViewModel}
     */
    private VM viewModel;

    @CallSuper
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ComponentProvider.getInstance().register(this);

        viewModel = createViewModel();
    }

    @CallSuper
    @Override
    protected void onStart() {
        super.onStart();

        if (viewModel != null) {
            viewModel.attachView(this);
        }
    }

    @CallSuper
    @Override
    protected void onStop() {
        super.onStop();

        if (viewModel != null) {
            viewModel.detachView();
        }
    }

    @CallSuper
    @Override
    protected void onDestroy() {
        super.onDestroy();

        ComponentProvider.getInstance().unregister(this);

        viewModel = null;
    }

    /**
     * Creates a new view model instance, if needed.
     * <p/>
     * This method will be called from {@link #onCreate(Bundle)}
     *
     * @return Returns new {@link BaseViewModel} instance.
     */
    protected abstract VM createViewModel();

    /**
     * Get the view model instance that are created for this activity.
     *
     * @return the view model instance that can be null.
     */
    @Nullable
    protected VM getViewModel() {
        return viewModel;
    }
}
