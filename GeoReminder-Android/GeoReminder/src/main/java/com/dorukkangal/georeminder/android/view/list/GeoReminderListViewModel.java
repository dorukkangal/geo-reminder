/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.view.list;

import android.databinding.Observable;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;

import com.dorukkangal.georeminder.android.dao.GeoReminderDao;
import com.dorukkangal.georeminder.android.dao.QueryCallBack;
import com.dorukkangal.georeminder.android.model.GeoReminder;
import com.dorukkangal.georeminder.android.view.base.BaseViewModel;

import java.util.List;

/**
 * @author Doruk Kangal
 */
public class GeoReminderListViewModel extends BaseViewModel<GeoReminderListView>
        implements QueryCallBack<List<GeoReminder>> {

    private ObservableArrayList<GeoReminderViewModel> reminders = new ObservableArrayList<>();

    private ObservableBoolean dataLoaded = new ObservableBoolean();

    @Override
    public void attachView(GeoReminderListView view) {
        super.attachView(view);
        GeoReminderDao.getGeoReminders(this);
    }

    @Override
    public void onQueryComplete(List<GeoReminder> results) {
        dataLoaded.set(true);

        reminders.clear();
        for (GeoReminder reminder : results) {
            final GeoReminderViewModel viewModel = new GeoReminderViewModel(reminder);
            viewModel.addOnPropertyChangedCallback(deleteReminderCallBack);
            reminders.add(viewModel);
        }
    }

    private Observable.OnPropertyChangedCallback deleteReminderCallBack = new Observable.OnPropertyChangedCallback() {

        @Override
        public void onPropertyChanged(Observable sender, int propertyId) {
            reminders.remove(sender);
        }
    };

    public ObservableArrayList<GeoReminderViewModel> getReminders() {
        return reminders;
    }

    public ObservableBoolean isDataLoaded() {
        return dataLoaded;
    }
}
