/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.view.base.binding;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.view.LayoutInflater;
import android.view.ViewGroup;

/**
 * @author Doruk Kangal
 */
public class BindableRecyclerViewListAdapter<T> extends RecyclerViewListAdapter<T, RecyclerViewBindingViewHolder> {

    private final int layoutResId;
    private final int bindingVariableId;

    public BindableRecyclerViewListAdapter(int layoutResId, int bindingVariableId) {
        super();

        this.layoutResId = layoutResId;
        this.bindingVariableId = bindingVariableId;
    }

    @Override
    public RecyclerViewBindingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final ViewDataBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                layoutResId,
                parent,
                false
        );

        return new RecyclerViewBindingViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerViewBindingViewHolder holder, int position) {
        holder.getBinding().setVariable(bindingVariableId, getItem(position));
    }
}
