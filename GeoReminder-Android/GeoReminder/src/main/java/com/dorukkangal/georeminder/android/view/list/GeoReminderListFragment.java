/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.view.list;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;

import com.dorukkangal.georeminder.android.BR;
import com.dorukkangal.georeminder.android.R;
import com.dorukkangal.georeminder.android.databinding.FragmentGeoReminderListBinding;
import com.dorukkangal.georeminder.android.objectgraph.ComponentProvider;
import com.dorukkangal.georeminder.android.view.base.BaseMvvmFragment;
import com.dorukkangal.georeminder.android.view.base.binding.BindableRecyclerViewListAdapter;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

/**
 * @author Doruk Kangal
 */
public class GeoReminderListFragment extends BaseMvvmFragment<GeoReminderListViewModel>
        implements GeoReminderListView {

    /**
     * New instance factory method.
     *
     * @return an instance of this fragment
     */
    public static GeoReminderListFragment newInstance() {
        return new GeoReminderListFragment();
    }

    @Override
    protected int getResourceLayoutId() {
        return R.layout.fragment_geo_reminder_list;
    }

    @Override
    public GeoReminderListViewModel createViewModel() {
        GeoReminderListViewModel viewModel = new GeoReminderListViewModel();
        ComponentProvider.getInstance().getActivityComponent(getActivity()).inject(viewModel);

        return viewModel;
    }

    @Override
    protected void initUserInterface(LayoutInflater inflater, View rootView) {
        final FragmentGeoReminderListBinding binding = FragmentGeoReminderListBinding.bind(rootView);
        binding.setViewModel(getViewModel());

        binding.fragmentGeoListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.fragmentGeoListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        binding.fragmentGeoListRecyclerView.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(getActivity()).build());
        binding.fragmentGeoListRecyclerView.setAdapter(
                new BindableRecyclerViewListAdapter<GeoReminderViewModel>(R.layout.list_item_geo_reminder, BR.viewModel));
    }
}
