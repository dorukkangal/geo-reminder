/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.view.base;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.view.View;

/**
 * A {@link BaseFragment} that uses an {@link BaseViewModel}
 * to implement a Model-View-ViewModel architecture.
 *
 * @param <VM> indicates the viewModel responsible to manage this fragment.
 * @author Doruk Kangal
 */
public abstract class BaseMvvmFragment<VM extends BaseViewModel>
        extends BaseFragment implements BaseView {

    /**
     * The view model class for this view.
     * Will be instantiated with {@link #createViewModel}
     */
    private VM viewModel;

    @CallSuper
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = createViewModel();
    }

    @CallSuper
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (viewModel != null) {
            viewModel.attachView(this);
        }
    }

    @CallSuper
    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (viewModel != null) {
            viewModel.detachView();
        }
    }

    @CallSuper
    @Override
    public void onDestroy() {
        super.onDestroy();

        viewModel = null;
    }

    @Nullable
    public abstract VM createViewModel();

    public VM getViewModel() {
        return viewModel;
    }
}
