/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.helper;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.dorukkangal.georeminder.android.objectgraph.ActivityScope;
import com.dorukkangal.georeminder.android.view.list.GeoReminderListActivity;
import com.dorukkangal.georeminder.android.view.map.GeoReminderMapActivity;

/**
 * Class used to navigate through the application.
 *
 * @author Doruk Kangal
 */
@ActivityScope
public class Navigator {

    private final Activity activity;

    public Navigator(Activity activity) {
        this.activity = activity;
    }

    public void navigateToGeoReminderListActivity(boolean clearBackStack) {
        startActivity(activity, GeoReminderListActivity.newIntent(activity), clearBackStack);
    }

    public void navigateToGeoReminderMapActivity() {
        startActivity(activity, GeoReminderMapActivity.newIntent(activity), false);
    }

    private void startActivity(@NonNull Activity activity, @NonNull Intent intent, boolean clearBackStack) {

        if (clearBackStack) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        }

        activity.startActivity(intent);
        activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
