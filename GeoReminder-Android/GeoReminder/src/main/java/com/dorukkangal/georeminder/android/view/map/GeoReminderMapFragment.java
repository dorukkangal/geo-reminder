/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.view.map;

import android.view.LayoutInflater;
import android.view.View;

import com.dorukkangal.georeminder.android.R;
import com.dorukkangal.georeminder.android.databinding.FragmentGeoReminderMapBinding;
import com.dorukkangal.georeminder.android.view.base.BaseMvvmFragment;

/**
 * @author Doruk Kangal
 */
public class GeoReminderMapFragment extends BaseMvvmFragment<GeoReminderMapViewModel>
        implements GeoReminderMapView {

    /**
     * New instance factory method.
     *
     * @return an instance of this fragment
     */
    public static GeoReminderMapFragment newInstance() {
        return new GeoReminderMapFragment();
    }

    @Override
    protected int getResourceLayoutId() {
        return R.layout.fragment_geo_reminder_map;
    }

    @Override
    public GeoReminderMapViewModel createViewModel() {
        return new GeoReminderMapViewModel();
    }

    @Override
    protected void initUserInterface(LayoutInflater inflater, View rootView) {
        final FragmentGeoReminderMapBinding binding = FragmentGeoReminderMapBinding.bind(rootView);
        binding.setViewModel(getViewModel());
    }
}
