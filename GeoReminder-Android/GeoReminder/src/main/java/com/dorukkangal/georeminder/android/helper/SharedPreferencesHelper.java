/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.helper;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

/**
 * @author Doruk Kangal
 */
@Singleton
public class SharedPreferencesHelper {

    private static final String KEY_SHARED_PREFERENCES_FILE = "geoReminderPreferences";

    private final SharedPreferences sharedPreferences;

    /**
     * Constructor.
     *
     * @param context context
     */
    public SharedPreferencesHelper(Context context) {
        sharedPreferences = context.getSharedPreferences(KEY_SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
    }

    /**
     * Puts string to application shared preferences.
     *
     * @param key   for preference
     * @param value of preference
     */
    public void putString(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    /**
     * Gets string from preferences.
     *
     * @param key for preference
     * @return value of preference
     */
    public String getString(String key) {
        return sharedPreferences.getString(key, "");
    }
}
