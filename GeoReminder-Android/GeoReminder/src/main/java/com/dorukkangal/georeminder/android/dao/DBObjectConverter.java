/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.dao;

import com.dorukkangal.georeminder.android.green.dao.GeoReminderEntity;
import com.dorukkangal.georeminder.android.model.GeoReminder;

/**
 * @author Doruk Kangal
 */
final class DBObjectConverter {

    /**
     * Don't instantiate.
     */
    private DBObjectConverter() {
    }

    public static GeoReminder toGeoReminder(GeoReminderEntity geoReminderEntity) {
        GeoReminder geoReminder = new GeoReminder();

        geoReminder.setId(geoReminderEntity.getId());
        geoReminder.setName(geoReminderEntity.getName());
        geoReminder.setLatitude(geoReminderEntity.getLatitude());
        geoReminder.setLongitude(geoReminderEntity.getLongitude());

        return geoReminder;
    }

    public static GeoReminderEntity toGeoReminderEntity(GeoReminder geoReminder) {
        GeoReminderEntity geoReminderEntity = new GeoReminderEntity();

        geoReminderEntity.setId(geoReminder.getId());
        geoReminderEntity.setName(geoReminder.getName());
        geoReminderEntity.setLatitude(geoReminder.getLatitude());
        geoReminderEntity.setLongitude(geoReminder.getLongitude());

        return geoReminderEntity;
    }
}
