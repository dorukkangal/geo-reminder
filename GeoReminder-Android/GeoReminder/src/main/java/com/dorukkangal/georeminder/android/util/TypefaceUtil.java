/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.util;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.dorukkangal.georeminder.android.R;

/**
 * @author Doruk Kangal
 */
public final class TypefaceUtil {

    private TypefaceUtil() {
    }

    public static Typeface getTypeface(String font, Context context) {

        Typeface tf = null;
        if (font != null) {
            tf = FontFactory.getInstance().getFont(context, font);
        }

        return tf;
    }

    public static Typeface getTypeFace(Context context, AttributeSet attrs, int defStyle) {

        Typeface typeFace = null;

        final TypedArray typedArray =
                context.getTheme().obtainStyledAttributes(attrs, R.styleable.GeoFonts, defStyle, 0);

        try {
            final String font = typedArray.getString(R.styleable.GeoFonts_custom_font);
            if (font != null) {
                typeFace = (TypefaceUtil.getTypeface(font, context));
            }
        } finally {
            typedArray.recycle();
        }
        return typeFace;
    }
}
