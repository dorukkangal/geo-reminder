/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.objectgraph;

import android.content.Context;
import android.support.annotation.NonNull;

import com.dorukkangal.georeminder.android.helper.SharedPreferencesHelper;
import com.dorukkangal.georeminder.android.helper.StringReader;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Application module which will live during the application lifecycle.
 *
 * @author Doruk Kangal
 */
@Module
class ApplicationModule {

    private final Context context;

    public ApplicationModule(Context applicationContext) {
        this.context = applicationContext;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return this.context;
    }

    @Provides
    @Singleton
    public StringReader provideStringHelper(@NonNull Context context) {
        return new StringReader(context);
    }

    @Provides
    @Singleton
    public SharedPreferencesHelper provideSharedPreferencesHelper(@NonNull Context context) {
        return new SharedPreferencesHelper(context);
    }
}
