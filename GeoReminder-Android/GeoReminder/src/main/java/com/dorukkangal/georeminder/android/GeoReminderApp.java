/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android;

import android.app.Application;

import com.dorukkangal.georeminder.android.dao.DBManager;
import com.dorukkangal.georeminder.android.dao.GeoReminderDao;
import com.dorukkangal.georeminder.android.model.GeoReminder;
import com.dorukkangal.georeminder.android.objectgraph.ComponentProvider;

/**
 * @author Doruk Kangal
 */
public class GeoReminderApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        initializeDatabase();

        ComponentProvider.getInstance().register(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        ComponentProvider.getInstance().unregister(this);
    }

    private void initializeDatabase() {
        DBManager.init(this);

        if (BuildConfig.DEBUG) {
            GeoReminderDao.deleteGeoReminders();
            GeoReminderDao.createGeoReminder(new GeoReminder(1, "Ev", 1.0, 1.0));
            GeoReminderDao.createGeoReminder(new GeoReminder(2, "Okul", 2.0, 2.0));
            GeoReminderDao.createGeoReminder(new GeoReminder(3, "Is", 3.0, 3.0));
            GeoReminderDao.createGeoReminder(new GeoReminder(4, "Market", 4.0, 4.0));
            GeoReminderDao.createGeoReminder(new GeoReminder(5, "Metro", 5.0, 5.0));
        }
    }
}
