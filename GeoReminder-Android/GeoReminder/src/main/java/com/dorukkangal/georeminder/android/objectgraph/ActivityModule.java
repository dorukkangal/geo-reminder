/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.objectgraph;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.dorukkangal.georeminder.android.helper.Navigator;

import dagger.Module;
import dagger.Provides;

/**
 * Activity module which will live during the activity lifecycle.
 *
 * @author Doruk Kangal
 */
@Module
class ActivityModule {

    private final Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    public Activity provideActivity() {
        return this.activity;
    }

    @Provides
    @ActivityScope
    public Navigator provideNavigator(@NonNull Activity activity) {
        return new Navigator(activity);
    }
}
