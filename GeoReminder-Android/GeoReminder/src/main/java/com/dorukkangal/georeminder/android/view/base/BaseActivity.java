/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.view.base;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.dorukkangal.georeminder.android.R;

/**
 * @author Doruk Kangal
 */
public abstract class BaseActivity extends AppCompatActivity {

    @CallSuper
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getContentResourceId());

        if (savedInstanceState == null) {

            final Fragment fragment = getContainedFragment();
            if (fragment != null) {
                addFragment(fragment);
            }
        }

        if (hasToolBar()) {
            final Toolbar toolbar = ((Toolbar) findViewById(R.id.toolbar));
            initToolBar(toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    protected int getContentResourceId() {
        return R.layout.activity_base;
    }

    protected abstract Fragment getContainedFragment();

    public int getBaseFrameLayoutId() {
        return R.id.activity_base_frame_layout;
    }

    protected void addFragment(Fragment fragment) {
        final FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(getBaseFrameLayoutId(), fragment).commit();
    }

    protected boolean hasToolBar() {
        return true;
    }

    protected void initToolBar(Toolbar toolbar) {

        final TextView textViewTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (hasToolbarTitle()) {
            textViewTitle.setVisibility(View.VISIBLE);
            textViewTitle.setText(getToolbarTitle());
        } else {
            textViewTitle.setVisibility(View.GONE);
        }
    }

    protected boolean hasToolbarTitle() {
        return true;
    }

    @StringRes
    protected String getToolbarTitle() {
        return String.valueOf(getTitle());
    }
}

