/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.objectgraph;

import android.app.Activity;
import android.app.Application;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Doruk Kangal
 */
public final class ComponentProvider {

    private static ComponentProvider instance;

    private ApplicationComponent applicationComponent;

    private Map<Class<? extends Activity>, ActivityComponent> activityComponents = new HashMap<>();

    public static ComponentProvider getInstance() {
        if (instance == null) {
            instance = new ComponentProvider();
        }
        return instance;
    }

    private ComponentProvider() {
    }

    public void register(Application application) {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(application)).build();
    }

    public void unregister(Application application) {
        applicationComponent = null;
    }

    public void register(Activity activity) {
        ActivityComponent activityComponent = applicationComponent
                .activityComponent(new ActivityModule(activity));
        activityComponents.put(activity.getClass(), activityComponent);
    }

    public void unregister(Activity activity) {
        activityComponents.remove(activity.getClass());
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    public ActivityComponent getActivityComponent(Activity activity) {
        return activityComponents.get(activity.getClass());
    }
}
