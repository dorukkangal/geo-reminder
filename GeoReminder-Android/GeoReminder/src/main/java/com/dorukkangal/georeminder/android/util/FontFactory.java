/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

/**
 * @author Doruk Kangal
 */
public class FontFactory {

    /**
     * Factory singleton.
     */
    private static FontFactory instance;

    /**
     * type face cache.
     */
    private HashMap<String, Typeface> fontMap = new HashMap<String, Typeface>();

    /**
     * Method that returns font factory.
     *
     * @return An instance.
     */
    public static FontFactory getInstance() {
        if (instance == null) {
            return instance = new FontFactory();
        } else {
            return instance;
        }
    }

    /**
     * Reads font from assets.
     *
     * @param context as an application or activity
     * @param font    full file name ex: roboto.tff
     * @return related Typeface
     */
    public Typeface getFont(Context context, String font) {
        Typeface typeface = fontMap.get(font);
        if (typeface == null) {
            try {
                typeface = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/" + font);
                fontMap.put(font, typeface);
            } catch (Exception e) {
                Log.e("FontFactory", "Could not get typeface: " + e.getMessage() + " with name: "
                        + font + "\n fonts should be stored in $PATH/assets/fonts "
                        + "and full file name has to be passed as argument");
                return null;
            }
        }
        return typeface;
    }

    public Typeface getFontFromRes(int resId, Context context) {
        Typeface tf = fontMap.get(String.valueOf(resId));

        if (tf == null) {

            InputStream is = null;
            try {
                is = context.getResources().openRawResource(resId);
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }

            final String outPath = context.getCacheDir() + "/tmp" + System.currentTimeMillis() + ".raw";

            try {
                final byte[] buffer = new byte[is.available()];
                final BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outPath));

                int l = 0;
                while ((l = is.read(buffer)) > 0) {
                    bos.write(buffer, 0, l);
                }

                bos.close();

                tf = Typeface.createFromFile(outPath);

                fontMap.put(String.valueOf(resId), tf);

                // clean up
                new File(outPath).delete();
            } catch (IOException e) {
                return null;
            }
        }
        return tf;
    }
}
