/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.view.base.binding;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import java.util.List;

/**
 * @author Doruk Kangal
 */
public final class BindingAdapters {

    /**
     * Default empty constructor
     */
    private BindingAdapters() {
    }

    /**
     * Binding adapter that sets item list to RecyclerView's adapter.
     *
     * @param recyclerView recyclerView
     * @param items        items
     */
    @BindingAdapter("items")
    public static void setItems(RecyclerView recyclerView, List items) {
        ((RecyclerViewListAdapter) recyclerView.getAdapter()).setItems(items);
    }

    /**
     * Binding adapter that sets image resource id to ImageView.
     *
     * @param imageView imageView
     * @param resId     the resource identifier of the drawable
     */
    @BindingAdapter("resourceId")
    public static void setResourceId(ImageView imageView, int resId) {
        imageView.setImageResource(resId);
    }
}
