/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.view.splash;

import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;

import com.dorukkangal.georeminder.android.R;
import com.dorukkangal.georeminder.android.view.base.BaseMvvmFragment;
import com.dorukkangal.georeminder.android.view.base.BaseViewModel;

/**
 * @author Doruk Kangal
 */
public class SplashScreenFragment extends BaseMvvmFragment {

    /**
     * New instance factory method.
     *
     * @return an instance of this fragment
     */
    public static SplashScreenFragment newInstance() {

        return new SplashScreenFragment();
    }

    @Override
    protected int getResourceLayoutId() {
        return R.layout.fragment_splash_screen;
    }

    @Override
    protected void initUserInterface(LayoutInflater inflater, View rootView) {
    }

    @Nullable
    @Override
    public BaseViewModel createViewModel() {
        return null;
    }
}
