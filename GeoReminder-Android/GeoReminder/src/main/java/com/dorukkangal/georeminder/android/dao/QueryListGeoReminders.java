/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.dao;

import com.dorukkangal.georeminder.android.green.dao.GeoReminderEntity;
import com.dorukkangal.georeminder.android.model.GeoReminder;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Doruk Kangal
 */
class QueryListGeoReminders implements Runnable {

    private final QueryCallBack callBack;

    /**
     * Const.
     *
     * @param callBack interface.
     */
    public QueryListGeoReminders(QueryCallBack<List<GeoReminder>> callBack) {
        this.callBack = callBack;
    }

    @Override
    public void run() {
        final List<GeoReminderEntity> result = DBManager.getDaoSession().getGeoReminderEntityDao().loadAll();
        final List<GeoReminder> geoReminderList = new ArrayList<>(result.size());

        for (GeoReminderEntity temp : result) {
            geoReminderList.add(DBObjectConverter.toGeoReminder(temp));
        }

        DBManager.getInstance().getHandler().post(new Runnable() {
            @Override
            public void run() {
                callBack.onQueryComplete(geoReminderList);
            }
        });
    }
}
