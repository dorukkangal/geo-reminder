/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.helper;

import android.content.Context;
import android.support.annotation.StringRes;

import javax.inject.Singleton;

/**
 * @author Doruk Kangal
 */
@Singleton
public class StringReader {

    private final Context context;

    /**
     * Constructor.
     *
     * @param context context
     */
    public StringReader(Context context) {

        this.context = context;
    }

    /**
     * Gets localized string from resources.
     *
     * @param resId Resource id for the string
     * @return The string data associated with the resource
     */
    public String getString(@StringRes int resId) {
        return context.getString(resId);
    }
}
