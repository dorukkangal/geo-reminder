/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.view.base;

import android.databinding.BaseObservable;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;

/**
 * A base implementation of a {@link BaseViewModel} that uses a <b>WeakReference</b> for referring
 * to the attached view.
 *
 * @param <V> view type.
 * @author Doruk Kangal
 */
public abstract class BaseViewModel<V extends BaseView> extends BaseObservable {

    private WeakReference<V> viewRef;

    @CallSuper
    public void attachView(V view) {
        this.viewRef = new WeakReference<>(view);
    }

    @CallSuper
    public void detachView() {
        if (viewRef != null) {
            viewRef.clear();
            viewRef = null;
        }
    }

    @Nullable
    protected V getView() {
        return viewRef == null ? null : viewRef.get();
    }
}
