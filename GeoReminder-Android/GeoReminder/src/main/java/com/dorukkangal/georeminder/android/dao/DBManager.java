/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Looper;

import com.dorukkangal.georeminder.android.green.dao.DaoMaster;
import com.dorukkangal.georeminder.android.green.dao.DaoSession;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Responsible class for managing DB.
 *
 * @author Doruk Kangal
 */
public final class DBManager {

    private static final String DB_NAME = "geo_reminder_db";

    private static final int INITIAL_THREAD_POOL_SIZE = 0;
    private static final int MAX_THREAD_POOL_SIZE = 1;

    private static final int THREAD_KEEP_ALIVE_TIME = 1;
    private static DaoSession daoSession;

    private final ThreadPoolExecutor threadPoolExecutor;
    private Handler handler;

    private static DBManager instance;

    /**
     * Don't instantiate.
     */
    private DBManager() {
        BlockingQueue<Runnable> dbBlockingQueue = new LinkedBlockingQueue<>();

        threadPoolExecutor = new ThreadPoolExecutor(
                INITIAL_THREAD_POOL_SIZE,
                MAX_THREAD_POOL_SIZE,
                THREAD_KEEP_ALIVE_TIME,
                TimeUnit.SECONDS,
                dbBlockingQueue
        );
        handler = new Handler(Looper.getMainLooper());
    }

    /**
     * Instantiate the db.
     *
     * @param context as activity or application.
     */
    public static void init(Context context) {
        final DaoMaster.DevOpenHelper databaseHelper = new DaoMaster.DevOpenHelper(context, DB_NAME, null);
        final SQLiteDatabase database = databaseHelper.getWritableDatabase();
        final DaoMaster daoMaster = new DaoMaster(database);
        daoSession = daoMaster.newSession();
    }

    /**
     * Dao session connected to db.
     *
     * @return active dao.
     */
    public static DaoSession getDaoSession() {

        if (daoSession == null) {
            throw new IllegalStateException("DBManager is not yet initialized");
        }

        return daoSession;
    }

    /**
     * An instance of manager.
     *
     * @return this.
     */
    public static DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    /**
     * Execute a given runnable on db manager's thread pool.
     *
     * @param runnable to run.
     */
    public void execute(Runnable runnable) {
        threadPoolExecutor.execute(runnable);
    }

    /**
     * Handler connected to UI thread's looper.
     *
     * @return handler
     */
    protected Handler getHandler() {
        return handler;
    }
}

