/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.view.list;

import android.databinding.Bindable;
import android.databinding.ObservableDouble;
import android.databinding.ObservableField;
import android.view.View;

import com.dorukkangal.georeminder.android.dao.GeoReminderDao;
import com.dorukkangal.georeminder.android.model.GeoReminder;
import com.dorukkangal.georeminder.android.view.base.BaseViewModel;

/**
 * @author Doruk Kangal
 */
public class GeoReminderViewModel extends BaseViewModel {

    @Bindable
    private final ObservableField<String> name = new ObservableField<>();

    @Bindable
    private final ObservableDouble latitude = new ObservableDouble();

    @Bindable
    private final ObservableDouble longitude = new ObservableDouble();

    private final GeoReminder geoReminder;

    public GeoReminderViewModel(final GeoReminder geoReminder) {
        this.geoReminder = geoReminder;

        this.name.set(geoReminder.getName());
        this.latitude.set(geoReminder.getLatitude());
        this.longitude.set(geoReminder.getLongitude());
    }

    public ObservableField<String> getName() {
        return name;
    }

    public ObservableDouble getLatitude() {
        return latitude;
    }

    public ObservableDouble getLongitude() {
        return longitude;
    }

    public GeoReminder getGeoReminder() {
        return geoReminder;
    }

    public void onDeleteClick(View view) {
        GeoReminderDao.deleteGeoReminder(this.geoReminder);
        notifyChange();
    }
}
