/*
 * Copyright 2016 Doruk Kangal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dorukkangal.georeminder.android.view.base.view;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import com.dorukkangal.georeminder.android.R;
import com.dorukkangal.georeminder.android.util.TypefaceUtil;

/**
 * @author Doruk Kangal
 */
public class GeoButton extends AppCompatButton {

    public GeoButton(Context context) {
        this(context, null);
    }

    public GeoButton(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.geoButtonStyle);
    }

    public GeoButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setTypeface(context, attrs, defStyle);
    }

    public void setTypeface(Context context, AttributeSet attrs, int defStyle) {
        setTypeface(TypefaceUtil.getTypeFace(context, attrs, defStyle));
    }
}
