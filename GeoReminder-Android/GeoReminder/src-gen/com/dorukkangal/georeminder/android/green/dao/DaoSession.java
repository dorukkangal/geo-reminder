package com.dorukkangal.georeminder.android.green.dao;

import android.database.sqlite.SQLiteDatabase;

import java.util.Map;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.AbstractDaoSession;
import de.greenrobot.dao.identityscope.IdentityScopeType;
import de.greenrobot.dao.internal.DaoConfig;

import com.dorukkangal.georeminder.android.green.dao.GeoReminderEntity;

import com.dorukkangal.georeminder.android.green.dao.GeoReminderEntityDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see de.greenrobot.dao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig geoReminderEntityDaoConfig;

    private final GeoReminderEntityDao geoReminderEntityDao;

    public DaoSession(SQLiteDatabase db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        geoReminderEntityDaoConfig = daoConfigMap.get(GeoReminderEntityDao.class).clone();
        geoReminderEntityDaoConfig.initIdentityScope(type);

        geoReminderEntityDao = new GeoReminderEntityDao(geoReminderEntityDaoConfig, this);

        registerDao(GeoReminderEntity.class, geoReminderEntityDao);
    }
    
    public void clear() {
        geoReminderEntityDaoConfig.getIdentityScope().clear();
    }

    public GeoReminderEntityDao getGeoReminderEntityDao() {
        return geoReminderEntityDao;
    }

}
